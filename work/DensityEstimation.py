# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# Example script for multidimensional probability density estimation using neural networks (multilayer perceptron)

import tensorflow as tf

import sys, os
sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""
from TensorFlowAnalysis import *

def main() : 

  # Ranges of variables 
  bounds = [ (-1., 1.), (-1., 1) ]

  # Definition of the phase space
  phsp = RectangularPhaseSpace( bounds )

  SetSeed(1)
  SetSinglePrecision()

  # Meta-parameters of the training procedure
  n_hidden = [ 32, 64, 16 ]  # number of neurons in hidden layers
  weight_penalty = 0.5       # penalty for L2 regularisation term
  learning_rate = 0.1        # Learning rate for Adam optimiser
  training_epochs = 2000     # Number of optimisation epochs
  titles = ["x", "y"]            # Titles of the variables
  outfile = "DensityEstimation"  # Prefix for the output files 
  print_step = 50                # Print cost function every 50 epochs
  display_step = 200             # Display fitted PDF and store to file every 200 epochs
  norm_size = 100000         # Size of the normalisation sample (not used here)

  threads = 8                # Maximum number of CPU threads

  n_input = len(bounds)   # number of input neurons
  (weights, biases) = CreateWeightsBiases(n_input, n_hidden)

  # Define the fitting model (generic multilayer perceptron)
  def model(x) : 
    return MultilayerPerceptron(x, weights, biases) + 1e-20 # to make sure PDF is always strictly positive

  # Placeholders for data and normalisation tensors
  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  # TF graphs for data and normalisation
  data_model = model(data_ph)
  norm_model = model(norm_ph)

  # Define loss and optimizer
  nll = UnbinnedNLL( data_model, Integral( norm_model ) ) + L2Regularisation(weights)*weight_penalty

  # Create Adam optimiser
  optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
  train_op = optimizer.minimize(nll)

  # True PDF
  def true_pdf(z) : 
    x = phsp.Coordinate(z, 0)
    y = phsp.Coordinate(z, 1)
    r = Sqrt(x**2 + y**2)
    return Exp(-0.5*(r-0.5)**2/0.1**2) + Exp(-0.5*r**2/0.1**2)

  # TF graph for true model
  true_model = true_pdf(data_ph)

  init = tf.global_variables_initializer()

  session_conf = tf.ConfigProto( intra_op_parallelism_threads=threads, inter_op_parallelism_threads=threads)

  with tf.Session(config = session_conf) as sess :
    sess.run(init)

    # Create normalisation sample as the square 200x200 grid over the phase space
    norm_sample = sess.run( phsp.RectangularGridSample( (200, 200) ) )
    print "Normalisation sample size = ", len(norm_sample)

    # Generate 10000 events according to true PDF
    data_sample = RunToyMC(sess, true_model, data_ph, phsp, 10000, 1. )

    # Create multidimensional display
    if display_step != 0 : 
      display = MultidimDisplay(data_sample, titles, bounds, pull = True)

#    norm_sample = sess.run( phsp.UniformSample( norm_size ) )
    print "Normalisation sample size = ", len(norm_sample)
    print norm_sample
    print "Data sample size = ", len(data_sample)
    print data_sample

    # Training cycle
    best_cost = 1e10
    for epoch in range(training_epochs):

        if display_step != 0 and (epoch % display_step == 0) : 

            norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )
            display.draw(norm_sample, norm_pdf, outfile + ".pdf")

        # Run optimization op (backprop) and cost op (to get loss value)
        _, c = sess.run([train_op, nll], feed_dict={data_ph: data_sample, norm_ph: norm_sample })

        # Display logs per epoch step
        if epoch % print_step == 0 : 
            s = "Epoch %d, cost %.9f" % (epoch+1, c)
            print s
            if c < best_cost : 
              best_cost = c
              np.save(outfile, sess.run( [ weights, biases ] ))
              f = open(outfile + ".txt", "w")
              f.write(s + "\n")
              f.close()

    print("Optimization Finished!")

if __name__ == "__main__" : 
  main()

import tensorflow as tf

from timeit import default_timer as timer
import sys, os

# Add path to TensorFlowAnalysis functions
sys.path.append("/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/")
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU

from ROOT import TH1F, TH2F, TCanvas, TFile, gStyle, gROOT, TPaveText, TLegend

from TensorFlowAnalysis import *

# Masses of initial and final state particles
mLc = 2.28646
mp  = 0.938272046
mK  = 0.493677
mPi = 0.13957018

# Blatt-Weisskopf radii
rLc = Const(5.) # For Lc
rR  = Const(1.5) # For all intermediate resonances

# Flag to control caching of helicity tensors
cache = True

# Calculate orbital momentum for a decay of a particle 
# of a given spin and parity to a proton (J^P=1/2+) and a pseudoscalar. 
# All the spins and total momenta are expressed in units of 1/2
def BaryonDecayOrbitalMomentum(spin, parity) : 
  l1 = (spin-1)/2     # Lowest possible momentum
  p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
  if p1 == parity : return l1
  return l1+1

# Return the sign in front of the complex coupling
# for amplitudes with baryonic intermediate resonances
# See Eq. (3), page 3 of LHCB-ANA-2015-072, 
# https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/ANA/2015/072/drafts/lb2dppi_aman_v3r4.pdf

def BaryonCouplingSign(spin, parity) : 
  jp =  1
  jd =  0
  pp =  1
  pd = -1
  s = 2*(((jp+jd-spin)/2+1) % 2)-1
  s *= (pp*pd*parity)
  return s

# Dictionary to describe the amplitude structure
# The dictionary has the structure 
#   "Resonance name" : Resonance
# where Resonance is again a dictionary describing a single resonance
# with the following properties: 
#   "channel"   : resonance channel 
#                     (0 for ppi resonance, 
#                      1 for Kpi resonance, 
#                      2 for Kp resonance)
#   "mass"      : Mass of the resonance 
#   "width"     : Width of the resonance. 
#                 Mass and width should be TF graphs, e.g. Const(...), FitParameter(...) etc.
#   "spin"      : spin of the resonance (in units of 1/2, i.e. 1 for spinor, 2 for vector etc.)
#   "parity"    : parity (1 or -1)
#   "couplings" : list of complex couplings. Two for baryons and four for meson resonances
#   "lineshape" : optional line shape class. By default, BreitWignerLineShape is used. 

#Define LASS lineshape parameters to be shared among spin-0 Kpi resonances
LASS_dict = {
    "a"         : FitParameter("a_LASS", 2.07, 0.5, 10., 0.01),
    "r"         : FitParameter("r_LASS", 3.32, 0., 100., 0.01),
}

#PDG-inspired model
resonances = {
#Non resonant component as S-wave Kpi configuration
  "KswaveNR" : {
    "channel"   : 3,
    "lineshape" : NonresonantLASSLineShape,
    "spin"      : 0,
    "parity"    : 1,
    "a"         : LASS_dict["a"],
    "r"         : LASS_dict["r"],
    "couplings" : [              
        Complex(FitParameter("ArKswave_1",  1., -5., 5., 0.002), FitParameter("AiKswave_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArKswave_2",  1., -5., 5., 0.002), FitParameter("AiKswave_2",  0.0, -5., 5., 0.002) ),
                  ]
  },
  "Kstar892" : {
    "channel"   : 1,
    "mass"      : Const(0.89176),
    "width"     : Const(0.0473), 
    "spin"      : 2, 
    "parity"    : -1,
    "couplings" : [              
        Complex(Const(1.),Const(0.)), 
        Complex(FitParameter("ArKst892_2",  1., -5., 5., 0.002), FitParameter("AiKst892_2",  0.0, -5., 5., 0.002) ),
        Complex(FitParameter("ArKst892_3",  1., -5., 5., 0.002), FitParameter("AiKst892_3",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArKst892_4",  1., -5., 5., 0.002), FitParameter("AiKst892_4",  0.0, -5., 5., 0.002) ),
                  ]
  },
  "Kstar1410" : {
    "channel"   : 1,
    "mass"      : Const(1.421),
    "width"     : Const(0.236), 
    "spin"      : 2, 
    "parity"    : -1,
    "couplings" : [              
        Complex(FitParameter("ArKst1410_1",  1., -5., 5., 0.002), FitParameter("AiKst1410_1",  0.0, -5., 5., 0.002) ),
        Complex(FitParameter("ArKst1410_2",  1., -5., 5., 0.002), FitParameter("AiKst1410_2",  0.0, -5., 5., 0.002) ),
        Complex(FitParameter("ArKst1410_3",  1., -5., 5., 0.002), FitParameter("AiKst1410_3",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArKst1410_4",  1., -5., 5., 0.002), FitParameter("AiKst1410_4",  0.0, -5., 5., 0.002) ),
                  ]
  },
  "Kstar1430" : {
    "channel"   : 3,
    "lineshape" : ResonantLASSLineShape,
    "mass"      : FitParameter("MKstar1430", 1.425, 1.375, 1.475, 0.001),
    "width"     : FitParameter("GKstar1430", 0.270, 0.190, 0.350, 0.001),
    "spin"      : 0, 
    "parity"    : 1, 
    "a"         : LASS_dict["a"],
    "r"         : LASS_dict["r"],
    "couplings" : [              
        Complex(FitParameter("ArKstar1430_1",  1., -5., 5., 0.002), FitParameter("AiKstar1430_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArKstar1430_2",  1., -5., 5., 0.002), FitParameter("AiKstar1430_2",  0.0, -5., 5., 0.002) ),
                  ]
  },
  "L1405" : {
    "channel"   : 2,
    "lineshape" : SubThresholdBreitWignerLineShape, 
    "mass"      : Const(1.4051), 
    "width"     : Const(0.0505), 
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1405_1",  1., -5., 5., 0.002), FitParameter("AiL1405_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1405_2",  1., -5., 5., 0.002), FitParameter("AiL1405_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1520" : {
    "channel"   : 2, 
    "mass"      : FitParameter("ML1520", 1.5195, 1.515, 1.523, 0.0001),
    "width"     : FitParameter("GL1520", 0.015, 0.01, 0.02, 0.0001), 
    "spin"      : 3, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1520_1",  1., -5., 5., 0.002), FitParameter("AiL1520_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1520_2",  1., -5., 5., 0.002), FitParameter("AiL1520_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1600" : {
    "channel"   : 2, 
    "mass"      : FitParameter("ML1600", 1.600, 1.550, 1.7, 0.002),
    "width"     : FitParameter("GL1600", 0.15, 0.05, 0.30, 0.001),
    "spin"      : 1, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1600_1",  1., -5., 5., 0.002), FitParameter("AiL1600_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1600_2",  1., -5., 5., 0.002), FitParameter("AiL1600_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1670" : {
    "channel"   : 2, 
    "mass"      : Const(1.670),
    "width"     : FitParameter("GL1670", 0.045, 0.025, 0.05, 0.0001), 
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1670_1",  1., -5., 5., 0.002), FitParameter("AiL1670_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1670_2",  1., -5., 5., 0.002), FitParameter("AiL1670_2",  0.0, -5., 5., 0.002) )
                  ]
  },
  "L1690" : {
    "channel"   : 2, 
    "mass"      : Const(1.690), 
    "width"     : Const(0.060), 
    "spin"      : 3, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1690_1",  1., -5., 5., 0.002), FitParameter("AiL1690_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1690_2",  1., -5., 5., 0.002), FitParameter("AiL1690_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1800" : {
    "channel"   : 2, 
    "mass"      : FitParameter("ML1800", 1.800, 1.720, 1.850, 0.001),
    "width"     : FitParameter("GL1800", 0.300, 0.200, 0.400, 0.001),
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1800_1",  1., -5., 5., 0.002), FitParameter("AiL1800_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1800_2",  1., -5., 5., 0.002), FitParameter("AiL1800_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1810" : {
    "channel"   : 2, 
    "mass"      : FitParameter("ML1810", 1.810, 1.750, 1.850, 0.001),
    "width"     : FitParameter("GL1810", 0.150, 0.050, 0.250, 0.001),
    "spin"      : 1, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1810_1",  1., -5., 5., 0.002), FitParameter("AiL1810_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1810_2",  1., -5., 5., 0.002), FitParameter("AiL1810_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1820" : {
    "channel"   : 2, 
    "mass"      : Const(1.820), 
    "width"     : Const(0.080), 
    "spin"      : 5, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1820_1",  1., -5., 5., 0.002), FitParameter("AiL1820_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1820_2",  1., -5., 5., 0.002), FitParameter("AiL1820_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1830" : {
    "channel"   : 2, 
    "mass"      : Const(1.820), 
    "width"     : FitParameter("GL1830", 0.080, 0.060, 0.110, 0.001),
    "spin"      : 5, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL1830_1",  1., -5., 5., 0.002), FitParameter("AiL1830_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1830_2",  1., -5., 5., 0.002), FitParameter("AiL1830_2",  0.0, -5., 5., 0.002) )
                  ]
  }, 
  "L1890" : {
    "channel"   : 2, 
    "mass"      : FitParameter("ML1890", 1.890, 1.850, 1.910, 0.0005),
    "width"     : FitParameter("GL1890", 0.130, 0.080, 0.200, 0.001),
    "spin"      : 3, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArL1890_1",  1., -5., 5., 0.002), FitParameter("AiL1890_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL1890_2",  1., -5., 5., 0.002), FitParameter("AiL1890_2",  0.0, -5., 5., 0.002) )
                  ]
  },
  "L2000" : {
    "channel"   : 2, 
    "mass"      : FitParameter("ML2000", 1.96, 1.9, 2.1, 0.0002), 
    "width"     : FitParameter("GL2000", 0.05, 0.02, 0.4, 0.0002), 
    "spin"      : 1, 
    "parity"    : -1, 
    "couplings" : [
        Complex(FitParameter("ArL2000_1",  1., -5., 5., 0.002), FitParameter("AiL2000_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL2000_2",  1., -5., 5., 0.002), FitParameter("AiL2000_2",  0.0, -5., 5., 0.002) )
                  ]
  },
  "L2020" : {
    "channel"   : 2, 
    "mass"      : FitParameter("ML2020", 2.02, 1.9, 2.1, 0.0002), 
    "width"     : FitParameter("GL2020", 0.05, 0.02, 0.4, 0.0002), 
    "spin"      : 7,
    "parity"    : 1,
    "couplings" : [
        Complex(FitParameter("ArL2020_1",  1., -5., 5., 0.002), FitParameter("AiL2020_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArL2020_2",  1., -5., 5., 0.002), FitParameter("AiL2020_2",  0.0, -5., 5., 0.002) )
                  ]
  },
  "D1232" : {
    "channel"   : 0, 
    "mass"      : Const(1.232),
    "width"     : Const(0.120),
    "spin"      : 3, 
    "parity"    : 1, 
    "couplings" : [
        Complex(FitParameter("ArD1232_1",  1., -5., 5., 0.002), FitParameter("AiD1232_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArD1232_2",  1., -5., 5., 0.002), FitParameter("AiD1232_2",  0.0, -5., 5., 0.002) )
                  ]
  },
}

# Return the list of resonance names from the dictionary above
def listComponentNames(res) : 
  return sorted(res.keys())

# Return the number of resonance components in the dictionary above
def numberOfComponents(res) : 
  return len(listComponentNames(res))

# Return the dictionary of amplitude components for a single resonance contribution
# as a dictionary key:value, where
#   key   : a tuple (mu, lambda_p)  (mu is a Lc spin projection, lambda_p is a proton helicity)
#   value : TF graph for the calculation of the corresponding amplitude for a single resonance
# Input parameters: 
#   res : a dictionary representing a single resonance (as in the "resonances" dictionary above)
#   var : dictionary of kinematic variables characterising a particular candidate
#   sw  : list of boolean "switches" to turn components on/off for fit fraction calculation
def getHelicityAmplitudes(res, var, sw) : 
  channel = res["channel"]

  # Determine lineshape function
  if "lineshape" in res : 
    lineShapeFunc = res["lineshape"]
  else : 
    lineShapeFunc = BreitWignerLineShape

  # Create the TF graph for lineshape depending on appropriate 
  # free parameters and kinematic variables
  if channel == 1 :
    if lineShapeFunc == FlatteLineShape :
      lineshape = lineShapeFunc(var["m2kpi"], res["mass"], res["g"], res["g"],mK,mPi,mK,mPi)
    else :
      lineshape = lineShapeFunc(var["m2kpi"], res["mass"], res["width"], 
                          mK, mPi, mp, mLc, rR, rLc, res["spin"]/2, res["spin"]/2-1)
  if channel == 2 : 
    lineshape = lineShapeFunc(var["m2pk"], res["mass"], res["width"], 
                          mp, mK, mPi, mLc, rR, rLc, BaryonDecayOrbitalMomentum(res["spin"], res["parity"]), (res["spin"]-1)/2)
  if channel == 0 : 
    lineshape = lineShapeFunc(var["m2ppi"], res["mass"], res["width"], 
                          mp, mPi, mK, mLc, rR, rLc, BaryonDecayOrbitalMomentum(res["spin"], res["parity"]), (res["spin"]-1)/2)
  if channel == 3 :
    if lineShapeFunc==NonresonantLASSLineShape :
      lineshape = lineShapeFunc(var["m2kpi"],res["a"],res["r"],mK,mPi)
    else :
      lineshape = lineShapeFunc(var["m2kpi"],res["mass"], res["width"],res["a"],res["r"],mK,mPi)

  ampl = {}

  # Loop over initial and final state polarisations
  # (amplitudes for them will be stored separately, and then added up incoherently)
  # For baryonic resonances, this follows Eq. (6), page 5 of the LHCb-ANA-2015-072, 
  # https://svnweb.cern.ch/cern/wsvn/lhcbdocs/Notes/ANA/2015/072/drafts/lb2dppi_aman_v3r4.pdf
  for pol_l in [-1, 1] : 
    for pol_p in [-1, 1] : 
      # Choose couplings depending on channel and polarisation
      if channel == 1 : # Kpi resonance
        if pol_p == -1 : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
          ampl[ (pol_l, pol_p) ] = coupling1*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  0, 0, 0, pol_p, cache = cache) + \
                                   coupling2*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l, -2, 0, 0, pol_p, cache = cache)
        else : 
          coupling1 = res["couplings"][2]
          coupling2 = res["couplings"][3]
          ampl[ (pol_l, pol_p) ] = coupling1*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  2, 0, 0, pol_p, cache = cache) + \
                                   coupling2*lineshape*sw*\
                  HelicityAmplitude3Body(var["kpi_theta_r"], var["kpi_phi_r"], var["kpi_theta_k"], var["kpi_phi_k"], 
                                         1, res["spin"], pol_l,  0, 0, 0, pol_p, cache = cache)

      if channel == 0 : # Delta++ resonance
        if pol_p == -1 : 
          sign = BaryonCouplingSign(res["spin"], res["parity"])
          coupling1 = res["couplings"][0]*sign
          coupling2 = res["couplings"][1]*sign
        else : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
        ampl[ (pol_l, pol_p) ] = sw*coupling1*lineshape*\
                                    HelicityAmplitude3Body(var["pk_theta_r"], var["pk_phi_r"], var["pk_theta_p"], var["pk_phi_p"], 
                                    1, res["spin"], pol_l,  1, 0, pol_p, 0, cache = cache) + \
                                 sw*coupling2*lineshape*\
                                    HelicityAmplitude3Body(var["pk_theta_r"], var["pk_phi_r"], var["pk_theta_p"], var["pk_phi_p"], 
                                    1, res["spin"], pol_l, -1, 0, pol_p, 0, cache = cache)

      if channel == 2 : # Lambda* resonance
        if pol_p == -1 : 
          sign = BaryonCouplingSign(res["spin"], res["parity"])
          coupling1 = res["couplings"][0]*sign
          coupling2 = res["couplings"][1]*sign
        else : 
          coupling1 = res["couplings"][0]
          coupling2 = res["couplings"][1]
        ampl[ (pol_l, pol_p) ] = sw*coupling1*lineshape*\
                                    HelicityAmplitude3Body(var["ppi_theta_r"], var["ppi_phi_r"], var["ppi_theta_p"], var["ppi_phi_p"], 
                                    1, res["spin"], pol_l,  1, 0, pol_p, 0, cache = cache) + \
                                 sw*coupling2*lineshape*\
                                    HelicityAmplitude3Body(var["ppi_theta_r"], var["ppi_phi_r"], var["ppi_theta_p"], var["ppi_phi_p"], 
                                    1, res["spin"], pol_l, -1, 0, pol_p, 0, cache = cache)

      if channel == 3 : # Non-resonant component
        if pol_p == -1 :
          coupling = res["couplings"][0]
        else :
          coupling = res["couplings"][1]

        d_term = Wignerd(var["thetap"], 1, pol_l, pol_p)
        ph = pol_l/2.*var["phip"]
        h = Complex(d_term*Cos(ph), d_term*Sin(ph))
        if cache : AddCacheableTensor( h )
        ampl[ (pol_l, pol_p) ] = sw*coupling*h*lineshape

  # Perform rotation of proton spin quantisation axis depending on channel
  if (channel == 3) :
    rot_angle = SpinRotationAngle( var["p4p"], var["p4k"], var["p4pi"], 1)
  else :
    rot_angle = SpinRotationAngle( var["p4p"], var["p4k"], var["p4pi"], channel)
  c = Complex( Cos(rot_angle/2.), Const(0.) )
  s = Complex( Sin(rot_angle/2.), Const(0.) )
  for pol_l in [-1, 1] : 
    a1 = ampl[ (pol_l, -1) ]
    a2 = ampl[ (pol_l,  1) ]
    ( ampl[ (pol_l, -1) ], ampl[ (pol_l, 1) ]) = ( c*a1 - s*a2, s*a1 + c*a2 )

  return ampl


# Create the phase space object
phsp = Baryonic3BodyPhaseSpace(mp, mK, mPi, mLc )

'''
# Read acceptance histogram
f = TFile.Open("Lc2pKpiEfficiency_20x20.root")
effhist = f.Get("H_Delta_Theta_ppi_mp")
effhist.SetDirectory(0)
f.Close()

# Create the TF graph for acceptance from the ROOT histogram
#effhist.Smooth()
effshape = RootHistShape(effhist)
'''
'''
#Defines efficiency parametrization in terms of Legendre polynomials
#Legendre poly max moments
a_max=2 #mpK
b_max=2 #Lambda*cosTheta
c_max=2 #p cosTheta
d_max=0 #p Phi
e_max=4 #chi

#Legendre poly coefficients
coeff=np.zeros((a_max+1,b_max+1,c_max+1,d_max+1,e_max+1))
#coeff=np.zeros((a_max+1,b_max+1))

f_coeff = open("Legendre_coefficients.txt", "r")
for a in range(a_max+1) :
  for b in range(b_max+1) :
    for c in range(c_max+1) :
      for d in range(d_max+1) :
        for e in range(e_max+1) :
          coeff[a][b][c][d][e] = float(f_coeff.readline())
'''
# Create the list of boolean switches for amplitude components
#  "+1" for an additional switch corresponding to background component
#switches = Switches(numberOfComponents(resonances)+1)
switches = Switches(numberOfComponents(resonances))

# Free background fraction
#bck = FitParameter("B",  0.5, 0., 1., 0.0002)

# Fixed background fraction
#bck = Const(0.5657)

# Polarization components
P = {
  "Px" : FitParameter("Px",  0., -1., 1., 0.0005),
  "Py" : FitParameter("Py",  0., -1., 1., 0.0005),
  "Pz" : FitParameter("Pz",  0., -1., 1., 0.0005),
}

# Function that returns the TF graph for a decay density for a phase space tensor "x"
def model_sig(x) : 

    # Calculate invariant masses at the phasespace point "x"
    m2pk  = phsp.M2ab(x)
    m2ppi = phsp.M2ac(x)
    m2kpi = phsp.M2bc(x)
    costhetap = phsp.CosThetaA(x)
    thetap = Acos(costhetap)
    phip = phsp.PhiA(x)
    phikpi = phsp.PhiBC(x)

    # Final state 4-momenta for a three-body decay
    p4p, p4k, p4pi = phsp.FinalStateMomenta(m2pk, m2kpi, costhetap, phip, phikpi)

    # Helicity angles for each of the three two-body decay channels
    #  theta_r and phi_r are the polar and azimuthal angles of Lc->RC decay
    #  theta_b and phi_b are the polar and azimuthal angles of the R->AB decay
    kpi_theta_r, kpi_phi_r, kpi_theta_k, kpi_phi_k = HelicityAngles3Body(p4k, p4pi, p4p)
    pk_theta_r, pk_phi_r, pk_theta_p, pk_phi_p     = HelicityAngles3Body(p4p, p4k, p4pi)
    ppi_theta_r, ppi_phi_r, ppi_theta_p, ppi_phi_p = HelicityAngles3Body(p4p, p4pi, p4k)

    # Dictionary of all kinematic variables characterising a candidate
    var = {
      "m2pk" : m2pk, 
      "m2kpi" : m2kpi, 
      "m2ppi" : m2ppi,
      "thetap" : thetap,
      "phip" : phip,
      "p4p" : p4p,
      "p4k" : p4k,
      "p4pi" : p4pi, 
      "kpi_theta_r" : kpi_theta_r, 
      "kpi_phi_r"   : kpi_phi_r, 
      "kpi_theta_k" : kpi_theta_k, 
      "kpi_phi_k"   : kpi_phi_k, 
      "pk_theta_r"  : pk_theta_r, 
      "pk_phi_r"    : pk_phi_r, 
      "pk_theta_p"  : pk_theta_p, 
      "pk_phi_p"    : pk_phi_p, 
      "ppi_theta_r" : ppi_theta_r, 
      "ppi_phi_r"   : ppi_phi_r, 
      "ppi_theta_p" : ppi_theta_p, 
      "ppi_phi_p"   : ppi_phi_p, 
    }

    ampls = {}

    # Start with zero amplitude
    for pol_l in [-1, 1] : 
      for pol_p in [-1, 1] : 
        ampls[ (pol_l, pol_p) ] = Complex(Const(0.), Const(0.))

    # Loop over all resonances, add up amplitudes corresponding to 
    # initial and final state polarisations coherently
    for i,n in enumerate(listComponentNames(resonances)) : 
      res = resonances[n]
      sw = Complex(switches[i], Const(0.))
      a = getHelicityAmplitudes(res, var, sw)
      for pol_l in [-1, 1] : 
        for pol_p in [-1, 1] : 
          ampls[ (pol_l, pol_p) ] += a[ (pol_l, pol_p) ]

    # Decay density is an incoherent sum of |a|^2 for each 
    # initial and final state polarisation 

#    density = Complex(Const(0.),Const(0.))
    density = Const(0.)

    # Unpolarized case
#    for pol_l in [-1, 1] : 
#      for pol_p in [-1, 1] : 
#        density += Density( ampls[ (pol_l, pol_p) ] )

    # Polarization allowed
    for pol_p in [-1, 1] : 
      density += tf.abs( Complex(Const(1.)+P["Pz"],Const(0.))*ampls[ (1, pol_p) ]*Conjugate(ampls[ (1, pol_p) ]) + Complex(Const(1.)-P["Pz"],Const(0.))*ampls[ (-1, pol_p) ]*Conjugate(ampls[ (-1, pol_p) ]) )
      density += Const(2.)*tf.real( Complex(P["Px"],P["Py"])*ampls[ (1, pol_p) ]*Conjugate(ampls[ (-1, pol_p) ]))

    return density
'''
    #Build efficiency function
    efficiency = Const(0.)
    mppk = 2.*(Sqrt(m2pk)-phsp.minab)/(phsp.maxab-phsp.minab)-1.
    for a in range(a_max+1) :
      for b in range(b_max+1) :
        for c in range(c_max+1) :
          for d in range(d_max+1) :
            for e in range(e_max+1) :
              efficiency += coeff[a][b][c][d][e]*Legendre(a,mppk)*Legendre(b,Cos(pk_theta_r))*Legendre(c,costhetap)*Legendre(d,phip/math.pi)*Legendre(e,phikpi/math.pi)
    return density*efficiency
'''

def model_bkg(x) : 

    return switches[-1]


# Placeholders for data and normalisation samples
data_ph = phsp.data_placeholder
norm_ph = phsp.norm_placeholder

# Placeholder for computing sig/bkg PDF normalisations
int_ph = phsp.norm_placeholder

# TF graphs for decay density as functions of data and normalisation placeholders
data_model_sig = model_sig(data_ph)
data_model_bkg = model_bkg(data_ph)
norm_model_sig = model_sig(norm_ph)
norm_model_bkg = model_bkg(norm_ph)

#No background
data_model = data_model_sig/Integral(norm_model_sig)

#Background included
#data_model = data_model_sig/Integral(norm_model_sig)*(1-bck)+data_model_bkg/Integral(norm_model_bkg)*bck

# Set random seed and create TF session
SetSeed(1)
sess = tf.Session()

# Initialise TF
init = tf.global_variables_initializer()
sess.run(init)

# Number of events to run on
nevents=80000

# Create normalisation sample
#norm_sample = sess.run(phsp.Filter(phsp.UnfilteredSample(nevents*10)))
#norm_sample1 = sess.run(phsp.Filter(phsp.UnfilteredSample(nevents*10)))

#print "Normalisation sample size = ", len(norm_sample)

# Open the normalisation ntuple
f1 = TFile.Open("/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/Lc2pKpi_MC_NR_lab_oldpid.root")
nt1 = f1.Get("nt")
norm_sample = sess.run(phsp.Filter(LoadNormSample(nt1, [ "m2pk", "m2kpi" , "costhetap", "phip", "phikpi"], -1 )))

# Open the data ntuple

f = TFile.Open("/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/Lc2pKpi_lab.root")
#f = TFile.Open("toyMC_Py=1.root")
nt = f.Get("nt")
data_sample1 = ReadNTuple(nt, [ "m2pk", "m2kpi" , "costhetap", "phip", "phikpi"], nevents ) 
data_sample = sess.run(phsp.Filter(data_sample1) )

# Create the TF graph for unbinned NLL from the data and normalisation graphs
#nll = UnbinnedNLL( data_model, Integral( norm_model ) )
nll = -tf.reduce_sum(tf.log(data_model))

# Read starting values of fit parameters 
#ReadFitResults(sess, "/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/results.txt")
ReadFitResults(sess, "/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/tmp_result.txt")

# Run minimisation and store results
'''
start = timer()
result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = True, runHesse = False, runMinos = False)
end = timer()
print "Time = "
print(end - start) 
print "Result = "
print result
WriteFitResults(result, "/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/results.txt")
'''
f.Close()

#Check normalization
int_sig = sess.run(Integral(norm_model_sig),feed_dict = {norm_ph : norm_sample})
#int_bkg = sess.run(Integral(norm_model_bkg),feed_dict = {norm_ph : norm_sample})

#print int_sig
#print int_bkg

#data_model = data_model_sig/int_sig*(1-bck)+data_model_bkg/int_bkg*bck
data_model = data_model_sig/int_sig

# Calculate and store fit fractions
ff = CalculateFitFractions(sess, data_model, data_ph, switches, norm_sample = norm_sample)
#WriteFitFractions(ff, listComponentNames(resonances) + ["Bkgr"], "fitfractions.txt")
WriteFitFractions(ff, listComponentNames(resonances), "/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/fitfractions.txt")

# Create toy MC sample corresponding to the fit result

majorant = EstimateMaximum(sess, data_model, data_ph, norm_sample )*1.01
#fit_sample = RunToyMC(sess, data_model, data_ph, phsp, nevents, majorant, chunk = 1000000, switches = switches)
fit_sample = RunToyMC(sess, data_model, data_ph, phsp, nevents, majorant, chunk = 100000, switches = switches, norm_sample = norm_sample)
f = TFile.Open("/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/toyresult.root", "RECREATE")
FillNTuple("toy", fit_sample, ["m2pk", "m2kpi" , "costhetap", "phip", "phikpi"] + [ "w%d" % (n+1) for n in range(len(switches)) ] )
f.Close()

# Plot fit results
# Dalitz plots data/toys
h1 = TH2F("h1", "", 200, phsp.minab-0.2, phsp.maxab+0.2, 200, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
h2 = TH2F("h2", "", 200, phsp.minab-0.2, phsp.maxab+0.2, 200, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
# m2pk projections
h3 = TH1F("h3", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
h4 = TH1F("h4", "", 100, phsp.minab-0.2, phsp.maxab+0.2 )
h_res_m2pk = [TH1F("h_res_m2pk"+str(i), "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) for i in range(len(switches))]
# m2kpi projections
h5 = TH1F("h5", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
h6 = TH1F("h6", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
h_res_m2kpi = [TH1F("h_res_m2kpi"+str(i), "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) for i in range(len(switches))]
# m2ppi projections
h7 = TH1F("h7", "", 100, phsp.minac-0.2, phsp.maxac+0.2 ) 
h8 = TH1F("h8", "", 100, phsp.minac-0.2, phsp.maxac+0.2 ) 
h_res_m2ppi = [TH1F("h_res_m2ppi"+str(i), "", 100, phsp.minac-0.2, phsp.maxac+0.2 ) for i in range(len(switches))]
# costhetap projections
h9 = TH1F("h9", "", 100, -1.-0.1, 1.+0.1 ) 
h10 = TH1F("h10", "", 100, -1.-0.1, 1.+0.1 )
h_res_costhetap = [TH1F("h_res_costhetap"+str(i), "", 100, -1.-0.1, 1.+0.1 ) for i in range(len(switches))]
# phip projections
h11 = TH1F("h11", "", 100, -math.pi-0.2, math.pi+0.2 ) 
h12 = TH1F("h12", "", 100, -math.pi-0.2, math.pi+0.2 )
h_res_phip = [TH1F("h_res_phip"+str(i), "", 100, -math.pi-0.2, math.pi+0.2 ) for i in range(len(switches))]
# phikpi projections
h13 = TH1F("h13", "", 100, -math.pi-0.2, math.pi+0.2 ) 
h14 = TH1F("h14", "", 100, -math.pi-0.2, math.pi+0.2 )
h_res_chi = [TH1F("h_res_chi"+str(i), "", 100, -math.pi-0.2, math.pi+0.2 ) for i in range(len(switches))]

for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])
    h9.Fill(d[2])
    h11.Fill(d[3])
    h13.Fill(d[4])

for d in phsp.M2ac(data_sample) : 
    h7.Fill(d)

for f in fit_sample : 
    m2ppi = phsp.msqsum-f[0]-f[1]
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])
    h10.Fill(f[2])
    h12.Fill(f[3])
    h14.Fill(f[4])
    h8.Fill(m2ppi)
    for i in range(len(switches)) :
      h_res_m2pk[i].Fill(f[0],f[5+i])
      h_res_m2kpi[i].Fill(f[1],f[5+i])
      h_res_costhetap[i].Fill(f[2],f[5+i])
      h_res_phip[i].Fill(f[3],f[5+i])
      h_res_chi[i].Fill(f[4],f[5+i])
      h_res_m2ppi[i].Fill(m2ppi,f[5+i])

gROOT.ProcessLine(".x /afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/lhcbStyle.C")

c = TCanvas("c","", 1200, 1200)
c.SetCanvasSize(1200,1200)
c.Divide(3, 3)
gStyle.SetPalette(1)
h3.SetLineColor(1)
h5.SetLineColor(1)
h7.SetLineColor(1)
h9.SetLineColor(1)
h11.SetLineColor(1)
h13.SetLineColor(1)
h4.SetLineColor(2)
h6.SetLineColor(2)
h8.SetLineColor(2)
h10.SetLineColor(2)
h12.SetLineColor(2)
h14.SetLineColor(2)
h1.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h1.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h2.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h2.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h3.GetXaxis().SetTitle("M^{2}(pK) [GeV^{2}]")
h5.GetXaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
h7.GetXaxis().SetTitle("M^{2}(p#pi) [GeV^{2}]")
h9.GetXaxis().SetTitle("cos #theta(p)")
h11.GetXaxis().SetTitle("#Phi(p)")
h13.GetXaxis().SetTitle("#chi")

#Labels and legend
#data_label = TPaveText(0.,0.,1.,1.)
#data_label.AddText("Data")
#model_label = TPaveText(0.,0.7,0.85,0.8)
#model_label.AddText("Model")
res_names = listComponentNames(resonances)
legend = TLegend(0.1,0.1,0.9,0.9)
legend.AddEntry(h3,"Data");
legend.AddEntry(h4,"Model");
for i in range(len(switches)) : legend.AddEntry(h_res_m2pk[i],res_names[i]);

#line colors
kRed=632; kMagenta=616; kBlue=600; kCyan=432; kGreen=416; kYellow=400; kOrange=800; kGray=920;
res_colors = [kRed+2,kRed-7,kMagenta,kMagenta+2,kMagenta-7,kBlue,kBlue+2,kBlue-7,kCyan,kCyan+3,kGreen,kGreen+2,kGreen+4,kGreen-6,kYellow+1,kOrange-3,kOrange+3,kGray+1,kBlue+4,1,kRed]

for i in range(len(switches)) :
  h_res_m2pk[i].SetLineColor(res_colors[i%21])
  h_res_m2kpi[i].SetLineColor(res_colors[i%21])
  h_res_m2ppi[i].SetLineColor(res_colors[i%21])
  h_res_costhetap[i].SetLineColor(res_colors[i%21])
  h_res_phip[i].SetLineColor(res_colors[i%21])
  h_res_chi[i].SetLineColor(res_colors[i%21])

c.cd(1); h1.Draw("colz");# data_label.Draw("same")
c.cd(2); h2.Draw("colz");# model_label.Draw("same")
c.cd(3); legend.Draw()
c.cd(4); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
for i in range(len(switches)) :
  h_res_m2pk[i].Scale(ff[i]*h3.Integral()/h_res_m2pk[i].Integral())
  h_res_m2pk[i].Draw("h same")

c.cd(5); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
for i in range(len(switches)) :
  h_res_m2kpi[i].Scale(ff[i]*h5.Integral()/h_res_m2kpi[i].Integral())
  h_res_m2kpi[i].Draw("h same")

c.cd(6); h7.Draw("e"); h8.Scale(h7.Integral()/h8.Integral()); h8.Draw("h same")
for i in range(len(switches)) :
  h_res_m2ppi[i].Scale(ff[i]*h7.Integral()/h_res_m2ppi[i].Integral())
  h_res_m2ppi[i].Draw("h same")

c.cd(7); h9.Draw("e"); h10.Scale(h9.Integral()/h10.Integral()); h10.Draw("h same")
for i in range(len(switches)) :
  h_res_costhetap[i].Scale(ff[i]*h9.Integral()/h_res_costhetap[i].Integral())
  h_res_costhetap[i].Draw("h same")

c.cd(8); h11.Draw("e"); h12.Scale(h11.Integral()/h12.Integral()); h12.Draw("h same")
for i in range(len(switches)) :
  h_res_phip[i].Scale(ff[i]*h11.Integral()/h_res_phip[i].Integral())
  h_res_phip[i].Draw("h same")

c.cd(9); h13.Draw("e"); h14.Scale(h13.Integral()/h14.Integral()); h14.Draw("h same")
for i in range(len(switches)) :
  h_res_chi[i].Scale(ff[i]*h13.Integral()/h_res_chi[i].Integral())
  h_res_chi[i].Draw("h same")

c.Update()
c.Print("/afs/cern.ch/user/d/dmarango/work/Lc2pKpi/Amplitude_Fit/Lc2pKpi.pdf")

'''
  "Kstar700" : {
    "channel"   : 3,
    "lineshape" : ResonantLASSLineShape,
    "mass"      : FitParameter("MKstar700", 0.824, 0.77, 0.87, 0.0005),
    "width"     : FitParameter("GKstar700", 0.478, 0.4, 0.6, 0.001),  
    "spin"      : 0,
    "parity"    : 1,
    "a"         : LASS_dict["a"],
    "r"         : LASS_dict["r"],
    "couplings" : [              
        Complex(FitParameter("ArKstar700_1",  1., -5., 5., 0.002), FitParameter("AiKstar700_1",  0.0, -5., 5., 0.002) ), 
        Complex(FitParameter("ArKstar700_2",  1., -5., 5., 0.002), FitParameter("AiKstar700_2",  0.0, -5., 5., 0.002) ),
                  ]
  },
'''

# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf

from timeit import default_timer as timer

import sys, math, os
sys.path.append("../")
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import TH1F, TH2F, TCanvas, gROOT, gStyle
from TensorFlowAnalysis import *

# Masses of the initial and final state particles 
mpi  = 0.13956995
md   = 1.8646
mk   = 0.493677
mb   = 5.2790

# Create Dalitz plot phase space 
dlz = DalitzPhaseSpace(md, mpi, mk, mb)

# Blatt-Weisskopf radial constants
dd = Const(5.)
dr = Const(1.5)

rbkst892 = 0.007
rbkst1430 = 0.038 
deltabkst892 = -1.17 
deltabkst1430 = -2.00

cache = False

# Auxiliary function to create the resonance of mass "m", width "w", spin "spin" in the 
# channel "ch", as a function of Dalitz plot variables vector "x"
def Resonance(x, m, w, spin, ch) : 
    if ch == 0 : 
      return BreitWignerLineShape(dlz.M2ac(x), m,  w, md, mk, mpi, mb, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2ac(x), dlz.M2ab(x), dlz.M2bc(x), Const(mb**2), Const(md**2), Const(mk**2), Const(mpi**2), spin, cache = cache)
    if ch == 1 : 
      return BreitWignerLineShape(dlz.M2bc(x), m,  w, mk, mpi, md, mb, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2bc(x), dlz.M2ac(x), dlz.M2ab(x), Const(mb**2), Const(mk**2), Const(mpi**2), Const(md**2), spin, cache = cache)
    if ch == 2 : 
      return BreitWignerLineShape(dlz.M2ab(x), m,  w, mpi, md, mk, mb, dr, dd, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2ab(x), dlz.M2bc(x), dlz.M2ac(x), Const(mb**2), Const(mpi**2), Const(md**2), Const(mk**2), spin, cache = cache)
    if ch == 5 : 
      return ResonantLASSLineShape(dlz.M2bc(x), Const(1.450), Const(0.400), Const(3.2), Const(0.9), mpi, mk)*\
             ZemachTensor(dlz.M2bc(x), dlz.M2ac(x), dlz.M2ab(x), Const(mb**2), Const(mk**2), Const(mpi**2), Const(md**2), spin, cache = cache)
    if ch == 6 : 
      return NonresonantLASSLineShape(dlz.M2bc(x), Const(3.2), Const(0.9), mpi, mk)*\
             ZemachTensor(dlz.M2bc(x), dlz.M2ac(x), dlz.M2ab(x), Const(mb**2), Const(mk**2), Const(mpi**2), Const(md**2), spin, cache = cache)
    if ch == 7 : 
      return DabbaLineShape(dlz.M2ab(x), Const(24.49), Const(0.1), Const(0.1), md, mpi)*\
             ZemachTensor(dlz.M2ab(x), dlz.M2bc(x), dlz.M2ac(x), Const(mb**2), Const(mpi**2), Const(md**2), Const(mk**2), spin, cache = cache)
    if ch == 8 : 
      return ExponentialNonResonantLineShape(dlz.M2ab(x), m, w, mpi, md, mk, mb, spin, spin, barrierFactor = False)*\
             ZemachTensor(dlz.M2ab(x), dlz.M2bc(x), dlz.M2ac(x), Const(mb**2), Const(mpi**2), Const(md**2), Const(mk**2), spin, cache = cache)
    return None

class B2DKpi_CA : 
  def __init__(self, fix = False) : 
    # Initial complex amplitudes for resonance components, (ampl, phase)
    a  = []

    a += [ (1.64829,  -1.57) ]  # K*(892)
    a += [ (0.47309,  -1.57) ]  # K*(1410)
    a += [ (8.06432,   0.79) ]  # LASS nonresonant
    a += [ (11.2418,   1.27) ]  # LASS resonant
    a += [ (0.10651,  -2.16) ]  # K2(1430)
    a += [ (56.85812, -2.64) ]  # D*0(2400)
    a += [ (506.2305,  2.40) ]  # Dabba
    a += [ (1536.364, -3.09) ]  # P-wave expo

    # Transform initial values of amplitudes into Cartesian fit parameters 
    self.c = []
    for n,ai in enumerate(a) : 
      if fix : 
        self.c += [ Complex(Const(ai[0]*math.cos(ai[1])), Const(ai[0]*math.sin(ai[1])) ) ]
      else : 
        self.c += [ Complex(FitParameter("a%dr" % n, ai[0]*math.cos(ai[1]), -10000., 10000., 0.01),
                            FitParameter("a%di" % n, ai[0]*math.sin(ai[1]), -10000., 10000., 0.01) ) ]

    self.switches = Switches(9)

  def amplitude(self, x) : 
    ampl  = Complex(Const(0.), Const(0.))

    ampl += CastComplex(self.switches[0])*Complex(Const(1.), Const(0.))*Resonance(x, Const(2.465), Const(0.046), 2, 2)

    ampl += CastComplex(self.switches[1])*self.c[0]*Resonance(x, Const(0.896), Const(0.051),  1, 1) # K*(892)
    ampl += CastComplex(self.switches[2])*self.c[1]*Resonance(x, Const(1.410),   Const(0.232), 1, 1) # K*(1410)
    ampl += CastComplex(self.switches[3])*self.c[2]*Resonance(x, None, None,                  0, 6) # LASS nonresonant
    ampl += CastComplex(self.switches[4])*self.c[3]*Resonance(x, None, None,                  0, 5) # LASS resonant
    ampl += CastComplex(self.switches[5])*self.c[4]*Resonance(x, Const(1.432), Const(0.109),  2, 1) # K2(1430)

    ampl += CastComplex(self.switches[6])*self.c[5]*Resonance(x, Const(2.360), Const(0.255),  0, 2) # D0(2400)
    ampl += CastComplex(self.switches[7])*self.c[6]*Resonance(x, None, None,  0, 7)                 # Dabba
    ampl += CastComplex(self.switches[8])*self.c[7]*Resonance(x, Const(0.), Const(0.88),  1, 8)     # P-wave Dpi expo

    return ampl

class B2DKpi_CS : 
  def __init__(self, rbkst892, deltabkst892, rbkst1430, deltabkst1430, fix = False, ) : 
    # Initial complex amplitudes for resonance components, (ampl, phase)
    a  = []

    a += [ (1.64829*rbkst892,  -1.57 + deltabkst892 ) ]  # K*(892)
    a += [ (0.10651*rbkst1430, -2.16 + deltabkst1430) ]  # K2(1430)

    # Transform initial values of amplitudes into Cartesian fit parameters 
    self.c = []
    for n,ai in enumerate(a) : 
      if fix : 
        self.c += [ Complex(Const(ai[0]*math.cos(ai[1])), Const(ai[0]*math.sin(ai[1])) ) ]
      else : 
        self.c += [ Complex(FitParameter("a%dr" % n, ai[0]*math.cos(ai[1]), -10000., 10000., 0.01),
                            FitParameter("a%di" % n, ai[0]*math.sin(ai[1]), -10000., 10000., 0.01) ) ]

    self.switches = Switches(2)

  def amplitude(self, x) : 
    ampl  = Complex(Const(0.), Const(0.))

    ampl += CastComplex(self.switches[0])*self.c[0]*Resonance(x, Const(0.896), Const(0.051),  1, 1) # K*(892)
    ampl += CastComplex(self.switches[1])*self.c[1]*Resonance(x, Const(1.432), Const(0.109),  2, 1) # K2(1430)

    return ampl


if __name__ == "__main__" : 

  target_ff = [ 0.231, 0.343, 0.007, 0.048, 0.051, 0.074, 0.193, 0.066, 0.089]

  ampl_ca = B2DKpi_CA(True)
  ampl_cs = B2DKpi_CS(rbkst892, deltabkst892, rbkst1430, deltabkst1430, True)

  ampl = ampl_cs

  # Signal model as a function of Dalitz plot vector "x"
  def model(x) :
    return Density( ampl.amplitude(x) ) 

  # Background model as a function of Dalitz plot vector "x". 
  # In this case, it is a constant density (==1)
  def bck_model(x) :
    return Ones(dlz.M2ab(x))

  bck_frac = 0.01

  # Set random seed for toy MC. 
  SetSeed(1)

  # Get placeholders for data and normalisation graphs
  data_ph = dlz.data_placeholder
  norm_ph = dlz.norm_placeholder

  # Create signal model graphs for data and normalisation
  sig_data_model = model(data_ph)
  sig_norm_model = model(norm_ph)

  # Create background model graphs for data and normalisation
  bck_data_model = bck_model(data_ph)
  bck_norm_model = bck_model(norm_ph)

  # Create normalisation graphs
  sig_norm = Integral( sig_norm_model )
  bck_norm = Integral( bck_norm_model )

  # Combined signal + background model, 90% signal and 10% background 
  exp_model = sig_data_model/sig_norm*(1.-bck_frac) + bck_data_model/bck_norm*bck_frac

  # Initialise TF
  sess = tf.Session()
  init = tf.global_variables_initializer()
  sess.run(init)

  # Generate normalisation sample, rectangular 500x500 sample in the Dalitz plot phase space
  norm_sample = sess.run( dlz.RectangularGridSample(500, 500) )

  # Calculate initial signal and background normalisations 
  init_sig_norm = sess.run( sig_norm, { norm_ph : norm_sample } )
  init_bck_norm = sess.run( bck_norm, { norm_ph : norm_sample } )

  # Create the graph for initial combined model. 
  # It is needed separately from "exp_model" because "exp_model"
  # depends on both data and normalisation placeholders, 
  # while EstimateMaximum and RunToyMC expect only a single placeholder
  # Thus the normalisation needs to be precalculated
  init_exp_model = sig_data_model/init_sig_norm*(1.-bck_frac) + bck_data_model/init_bck_norm*bck_frac

  # Calculate the maximum of the density
#  majorant = EstimateMaximum(sess, init_exp_model, data_ph, norm_sample)*1.2
  majorant = 200.
  print "Maximum = ", majorant
  data_sample = RunToyMC( sess, init_exp_model, data_ph, dlz, -10, majorant, chunk = 2000000)

  # Create the graph for unbinned negative log likelihood. 
  # The density is normalised by construction, so no need to 
  # provide the graph for normalisation
  nll = UnbinnedNLL( exp_model, Const(1.) )
  # Minimise NLL
  start = timer()
#  RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample}, useGradient = True )
  end = timer()
  print(end - start) 

  ff = CalculateFitFractions(sess, sig_data_model, data_ph, ampl.switches, norm_sample = norm_sample )
  WriteFitFractions(ff, ["D22460", "Kst892", "Kst1410", "LASS", "K01430", "K21430", "D02400", "DABBA", "DpiNR"], "fitfractions_b2dkpi.txt")

  # Create the graphs for fitted signal and background normalisations,
  # and the fitted combined model, in the same way as for the initial model
  fit_sig_norm = sess.run( sig_norm, { norm_ph : norm_sample } )
  fit_bck_norm = sess.run( bck_norm, { norm_ph : norm_sample } )
  fit_exp_model = sig_data_model/fit_sig_norm*(1.-bck_frac) + bck_data_model/fit_bck_norm*bck_frac

  # Geterate toy MC sample corresponding to the fitted result
  fit_sample = RunToyMC(sess, fit_exp_model, data_ph, dlz, -20, majorant, chunk = 2000000, switches = ampl.switches)

  # Plot the initial and fitted distributions
  h1 = TH2F("h1", "", 200, dlz.minab-0.2, dlz.maxab+0.2, 200, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h2 = TH2F("h2", "", 200, dlz.minab-0.2, dlz.maxab+0.2, 200, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 

  h3 = TH1F("h3", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h4 = TH1F("h4", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h5 = TH1F("h5", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 
  h6 = TH1F("h6", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 
  h7 = TH1F("h7", "", 100, dlz.minac-0.2, dlz.maxac+0.2 ) 
  h8 = TH1F("h8", "", 100, dlz.minac-0.2, dlz.maxac+0.2 ) 

  for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

  for d in sess.run(dlz.M2ac(data_ph), feed_dict = {data_ph : data_sample} ) : 
    h7.Fill(d)

  for f in fit_sample : 
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])

  for f in sess.run(dlz.M2ac(data_ph), feed_dict = {data_ph : fit_sample} ) : 
    h8.Fill(f)

  gROOT.ProcessLine(".x lhcbstyle2.C")

  c = TCanvas("c","", 900, 600)
  c.Divide(3, 2)
  gStyle.SetPalette(107)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  h8.SetLineColor(2)
  h1.GetXaxis().SetTitle("M^{2}(D#pi) [GeV^{2}]")
  h1.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
  h2.GetXaxis().SetTitle("M^{2}(D#pi) [GeV^{2}]")
  h2.GetYaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
  h3.GetXaxis().SetTitle("M^{2}(D#pi) [GeV^{2}]")
  h5.GetXaxis().SetTitle("M^{2}(K#pi) [GeV^{2}]")
  h7.GetXaxis().SetTitle("M^{2}(DK) [GeV^{2}]")

  c.cd(1); h1.Draw("zcol")
  c.cd(2); h2.Draw("zcol")
  c.cd(4); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
  c.cd(5); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
  c.cd(6); h7.Draw("e"); h8.Scale(h7.Integral()/h8.Integral()); h8.Draw("h same")
  c.Update()
  c.Print("B2DKpi.pdf")

  print(end - start) 


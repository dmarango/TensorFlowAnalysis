#!/bin/python

import numpy as np
from array import array

#Unbinned GoF test
#follows ref arXiv:1006.3019v2
def I_sumk_p_MixedSample(p, data, mc, p_isdata, w = 1., nk = 10): 
    """
    data array shape -  (ndata,n_dim) 
    mc  array shape  - (nmcs,n_dim) 
    point p (belongs to data or mc) with array shape - (1,n_dim)
    weight w = 1. if x and y have same domain if not is given by RMS of the sample (ref arXiv:1006.3019v2)
    """
    #1st calculate nk nearest neighbors points for a given point p
    nearest_data_pts = None; nearest_mc_pts  = None
    if p_isdata: #remove the point from corresponding sample
        nearest_data_pts = np.sort(np.sum((p - data)**2./w**2., axis=1))[1:nk+1] #shape (ndata,) but choose nk
        nearest_mc_pts  = np.sort(np.sum((p - mc)**2./w**2., axis=1))[0:nk]    #shape (nmc,) but choose nk
    else:
        nearest_data_pts = np.sort(np.sum((p - data)**2./w**2., axis=1))[0:nk] 
        nearest_mc_pts  = np.sort(np.sum((p - mc)**2./w**2., axis=1))[1:nk+1] 
    neighbors = np.sort(np.concatenate([nearest_data_pts,nearest_mc_pts]))[:nk] 
    #For a point, given that it belongs to a class, calculate the number of nearby by points of same class
    i_sumk_p = 0.
    for k in neighbors: #this is should work even in case of same dist b/w two classes 
        if k in nearest_data_pts and p_isdata: 
            i_sumk_p += 1.
        elif k in nearest_mc_pts and not p_isdata: 
            i_sumk_p += 1.
    return i_sumk_p

#Unbinned GoF test
def get_pvalue_MixedSample(data, mc, n_k = 10):
    """
    data array shape -  (ndata,n_dim) 
    mc  array shape  - (nmcs,n_dim) 
    n_k: nearest neighbor, there is limit on n_mc and n_k that this method imposes i.e. n_mc = 10*ndata, nk=10 was optimal (factor 10 makes it worse)
    """
    n_mc = len(mc); n_data = len(data); n_tot = n_mc + n_data
    #get weight in each dim
    sample = np.concatenate((data,mc), axis=0) #combine sample, shape = (nmc+ndata, n_dim)
    weight = np.sqrt(1./len(sample) * np.sum(sample**2., axis=0))#RMS, shape = (n_dim,)
    #calculate test statistic and GoF
    mu_T    = (n_mc*(n_mc -1.) + n_data*(n_data-1.))/(n_tot*(n_tot-1.))
    sigma_T = np.sqrt(1./(n_tot*n_k) * (n_mc*n_data/n_tot**2. + (2.*n_mc*n_data/n_tot**2.)**2.))
    T       =  1./(n_k*len(sample)) * sum([I_sumk_p_MixedSample(p,data,mc,p_isdata=False,w=weight,nk=n_k) for p in mc]) 
    T      +=  1./(n_k*len(sample)) * sum([I_sumk_p_MixedSample(p,data,mc,p_isdata=True,w=weight,nk=n_k)  for p in data])
    T_observed    = (T - mu_T)/sigma_T
    #get pvalue with a std gaussian dist.
    np.random.seed(2); std_norm = np.random.normal(size=10000)
    p_val = len(std_norm[std_norm>T_observed])/float(len(std_norm))
    print 'p value', p_val
    return p_val

#Unbinned GoF test
def phi_EnergyTest(sample, sample1 = None, w = 1., sigma = 0.01): 
    """
    sample shape - (npoints,n_dim) 
    weight w = 1. if x and y have same domain if not is given by RMS of the sample (ref arXiv:1006.3019v2)
    """
    if type(sample1) == np.ndarray: #np.sum axis=1 gives shape (n_sample1,)
        return sum([np.sum(np.exp(-0.5*np.sum((p - sample1)**2./w**2.,axis=1)/sigma**2.)) for p in sample]) 
    else: #NB: For this phi we have in the sum i<j hence the below gymnastics- the np.sum axis=1 gives shape (npoints-i+1,)
        return sum([np.sum(np.exp(-0.5*np.sum((p - sample[i+1:])**2./w**2.,axis=1)/sigma**2.)) for i, p in enumerate(sample) if i+1 != len(sample)])

#Unbinned GoF test
def get_Tstat_EnergyTest(data, mc, sample):
    #get weight for data, mc and combined sample
    w_data = np.sqrt(1./len(data) * np.sum(data**2.,axis=0))#RMS, shape = (n_dim,)
    w_mc   = np.sqrt(1./len(mc) * np.sum(mc**2.,axis=0))      
    w_comb = np.sqrt(1./len(sample) * np.sum(sample**2.,axis=0))    
    #make phi funcs
    sum_phi_data  = phi_EnergyTest(data, w = w_data)
    sum_phi_mc    = phi_EnergyTest(mc,  w = w_mc)
    sum_phi_comb  = phi_EnergyTest(data, sample1 = mc, w = w_comb)
    #make test statistic
    nd = len(data); nmc = len(mc)
    T = 1./(nd*(nd-1.)) * sum_phi_data + 1./(nmc*(nmc-1.)) * sum_phi_mc - 1./(nmc*nd) * sum_phi_comb
    return T

#Unbinned GoF test
def get_pvalue_EnergyTest(data, mc, npermuations = 100):
    sample = np.concatenate((data,mc), axis=0)
    T_observed = get_Tstat_EnergyTest(data, mc, sample)
    #do permutation test
    T_values = np.zeros(npermuations)
    for i in range(len(T_values)):
        np.random.seed(i)
        newsample = np.random.permutation(sample)
        newdata   = newsample[:len(data)]
        newmc     = newsample[len(data):]
        T_values[i] = get_Tstat_EnergyTest(newdata, newmc, newsample)
    ##calculate p_value
    p_val = len(T_values[T_values>T_observed])/float(len(T_values))
    print 'p value', p_val
    return p_val

#Unbinned GoF test
##Test with this model
#mean = [1.,2.]
#cov  = [[1, 0], [0, 1]]
#data = np.random.multivariate_normal(mean,cov,1000)
#print data.shape
#same parent sample
mean = [1.,1.5]
cov  = [[1.4, 0.5], [0.5, 1]]
mc   = np.random.multivariate_normal(mean,cov,10*1000)
print mc.shape
##different parent sample
#mc = np.random.rand(10*1000, 2)
#print mc.shape
print get_pvalue_MixedSample(data, mc)
print get_pvalue_EnergyTest(data, mc)

import sys, os, math
sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

if __name__ == "__main__" : 
  
  # Masses of the initial and final state particles
  mb   = 5.279
  mdst = 2.010
  mds  = 1.940
  mpi  = 0.139

  # Radial constants for Blatt-Weisskopf form factors
  db = Const(5.)
  dr = Const(1.5)

  # Set random seed
  SetSeed(1)

  # Imaginary "i"
  I = Complex(Const(0.), Const(1.))

  # Fit phase space is the direct product of Dalitz and 2D angular phase space (in cos(theta) vs. phi)
  dalitzPhsp = DalitzPhaseSpace(mdst, mpi, mds, mb)
  angularPhsp = RectangularPhaseSpace( ((-1., 1.), (-math.pi, math.pi)) )
  phsp = CombinedPhaseSpace(dalitzPhsp, angularPhsp)

  # Couplings for "10" and "00" helicities
  c10 = Complex( FitParameter("a10r", -1., -10., 10.), FitParameter("a10i", 1., -10., 10.) )
  c00 = Complex(Const(1.), Const(0.)),

  a = { -2 : c10, 0 : c00, 2 : c10 }  # Dictrionary of couplings, "10" and "-10" are the same due to parity conservation

  def model(x) : 
    """
      Description of the decay density as a function of the vector of input 
      parameters x.
    """
    dlz = phsp.Data1(x)  # Dalitz plot variables
    ang = phsp.Data2(x)  # Angles
    m2dstpi = dalitzPhsp.M2ab(dlz)
    cos_th_dst = dalitzPhsp.CosHelicityAB(dlz)  # D* helicity angle
    cos_th_d = angularPhsp.Coordinate(ang, 0)   # D helicity angle
    phi_d = angularPhsp.Coordinate(ang, 1)      # azimuthal angle of D*->Dpi (or angle between Dspi and D*->Dpi planes)

    j_dstst = 2  # D** spin == 1 (spins are always doubled)

    # Construct the amplitude of only one intermediate resonance, D1(2420)->D*pi

    ampl = Complex(Const(0.), Const(0.))
    # Loop over D* helicities (allowed ones are -1, 0, 1)
    for lambda_d in [-2, 0, 2] : 
      # Helicity term in the amplitude (for J^P=1+)
      h = CastComplex(Wignerd(Acos(cos_th_dst), j_dstst, 0, lambda_d))*Exp(I*CastComplex(lambda_d/2.*phi_d))*CastComplex(Wignerd(Acos(cos_th_d), 2, lambda_d, 0))
      # Add terms to the total amplitude multiplied by coupling
      ampl += h*a[lambda_d]
    # Dynamical term (Breit-Wigner) for D1(2420) state (J^P=1+)
    bw = BreitWignerLineShape(m2dstpi, Const(2.420), Const(0.0317), mdst, mpi, mds, mb, dr, db, 0, 1)
    ampl *= bw
 
    # Density is the abs value squared of the amplitude
    return Density(ampl)

  # Initialise TF
  init = tf.global_variables_initializer()
  sess = tf.Session()
  sess.run(init)

  # Placeholders for data and normalisation samples
  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  # TF graphs for decay density as functions of data and normalisation placeholders
  data_model = model(data_ph)
  norm_model = model(norm_ph)

  # Uniform mormalisation sample
  norm_sample = sess.run( phsp.UniformSample(1000000) )

  # Calculate the maximum of PDF for accept-reject method
  majorant = EstimateMaximum(sess, norm_model, norm_ph, norm_sample)*1.5
  print "Maximum = ", majorant

  # Run toy MC with the model defined above
  data_sample = RunToyMC( sess, norm_model, norm_ph, phsp, 100000, majorant, chunk = 1000000)

  # Calculate D* helicity angle which is a function of Dspi inv. mass
  helangle = np.stack( [ sess.run( dalitzPhsp.CosHelicityAB(phsp.Data1(data_sample)) ) ], axis = 1 )
  # Add D* helcitiy angle to the generated toy MC sample
  array = np.concatenate( [data_sample, helangle], axis = 1)

  # Store the result to ROOT file
  FillRootFile("toy.root", array, [ "m2dstpi", "m2dspi", "costhd", "phid", "costhdst" ] )

  # Negative log likelihood for the fit
  nll = UnbinnedNLL( data_model, Integral( norm_model ) )

  # Perform Minuit minimisation
  result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = True )

  # Write results to text file
  WriteFitResults(result, "result.txt")

# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf

from timeit import default_timer as timer

import sys, os
sys.path.append("../")

from ROOT import TH1F, TH2F, TCanvas, TFile

from TensorFlowAnalysis import *

def OrbitalMomentum(spin, parity) : 
  l1 = (spin-1)/2     # Lowest possible momentum
  p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
  if p1 == parity : return l1
  return l1+1

def CouplingSign(spin, parity) : 
  jp =  1
  jd =  0
  pp =  1
  pd = -1
  s = 2*(((jp+jd-spin)/2+1) % 2)-1
  s *= (pp*pd*parity)
  return s

if __name__ == "__main__" : 

  cache = False
  norm_grid = 400
  toy_sample = 10000
  gradient = True
  backend = "/gpu:0"

  for i in sys.argv[1:] :
    if i == "--nogpu" : 
      os.environ["CUDA_VISIBLE_DEVICES"] = ""
      backend = "/cpu:0"
    if i == "--cache" : 
      cache = True
    if i == "--100k" : 
      norm_grid = 800
      toy_sample = 100000
    if i == "--nograd" : 
      gradient = False

  mlb = 5.620
  md  = 1.865
  mpi = 0.140
  mp  = 0.938

  phsp = Baryonic3BodyPhaseSpace(md, mp, mpi, mlb, mabrange = (0., 3.) )

  mass_lcst   = Const(2.88153)
  width_lcst  = Const(0.0058)

  mass_lcx    = Const(2.857)
  width_lcx   = Const(0.060)

  mass_lcstst   = Const(2.945)
  width_lcstst  = Const(0.026)

  mass0 = Const(3.)

  db = Const(5.)
  dr = Const(1.5)

  alpha12p = FitParameter("alpha12p", 2.3, 0., 10., 0.01)
  alpha12m = FitParameter("alpha12m", 1.0, 0., 10., 0.01)
  alpha32p = FitParameter("alpha32p", 2.5, 0., 10., 0.01)
  alpha32m = FitParameter("alpha32m", 2.6, 0., 10., 0.01)

  couplings = [
    (
        Complex(Const(1.), Const(0.) ), 
        Complex(Const(0.), Const(0.) ) 
    ), 
    (
        Complex(FitParameter("ArX1", -0.38, -10., 10., 0.01), FitParameter("AiX1",  0.86, -10., 10., 0.01) ), 
        Complex(FitParameter("ArX2",  6.59, -10., 10., 0.01), FitParameter("AiX2", -0.38, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar29401",  0.53, -10., 10., 0.01), FitParameter("Ai29401", 0.14, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar29402", -1.24, -10., 10., 0.01), FitParameter("Ai29402", 0.02, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar12p1",  0.05, -10., 10., 0.01), FitParameter("Ai12p1",  0.23, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar12p2", -0.16, -10., 10., 0.01), FitParameter("Ai12p2", -2.86, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar12m1",  1.17, -10., 10., 0.01), FitParameter("Ai12m1", 0.76, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar12m2", -2.55, -10., 10., 0.01), FitParameter("Ai12m2", 3.86, -10., 10., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar32p1",  0., -100., 100., 0.01), FitParameter("Ai32p1",  0., -100., 100., 0.01) ), 
        Complex(FitParameter("Ar32p2",  0., -100., 100., 0.01), FitParameter("Ai32p2",  0., -100., 100., 0.01) )
    ), 
    (
        Complex(FitParameter("Ar32m1",  0.95, -10., 10., 0.01), FitParameter("Ai32m1", -0.45, -10., 10., 0.01) ), 
        Complex(FitParameter("Ar32m2", -2.27, -10., 10., 0.01), FitParameter("Ai32m2",  0.95, -10., 10., 0.01) )
    )
  ]

  switches = Switches(len(couplings))

  x = phsp.Placeholder()

  def pdf(x) : 

    m2dp  = phsp.M2ab(x)
    m2ppi = phsp.M2bc(x)

    p4d, p4p, p4pi = phsp.FinalStateMomenta(m2dp, m2ppi, 0., 0., 0.)

    resonances = [
      ( BreitWignerLineShape(m2dp, mass_lcst, width_lcst, md, mp, mpi, mlb, dr, db, OrbitalMomentum(5, 1), 2, False), 5, 1, 
        couplings[0][0], couplings[0][1]
      ), 
      ( BreitWignerLineShape(m2dp, mass_lcx, width_lcx, md, mp, mpi, mlb, dr, db, OrbitalMomentum(3, 1), 1, False), 3, 1, 
        couplings[1][0], couplings[1][1]
      ), 
      ( BreitWignerLineShape(m2dp, mass_lcstst, width_lcstst, md, mp, mpi, mlb, dr, db, OrbitalMomentum(3, -1), 1, False), 3, -1, 
        couplings[2][0], couplings[2][1]
      ), 
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha12p, md, mp, mpi, mlb, OrbitalMomentum(1, 1), 0, False), 1, 1, 
        couplings[3][0], couplings[3][1]
      ), 
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha12m, md, mp, mpi, mlb, OrbitalMomentum(1,-1), 0, False), 1,-1, 
        couplings[4][0], couplings[4][1]
      ),
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha32p, md, mp, mpi, mlb, OrbitalMomentum(3, 1), 1, False), 3, 1, 
        couplings[5][0], couplings[5][1]
      ), 
      ( ExponentialNonResonantLineShape(m2dp, mass0, alpha32m, md, mp, mpi, mlb, OrbitalMomentum(3,-1), 1, False), 3,-1, 
        couplings[6][0], couplings[6][1]
      ), 
    ]

    density = Const(0.)
    p4lb = p4d + p4p + p4pi

    spinor_p  = DiracSpinors(1, p4p,  mp)
    spinor_lb = DiracSpinors(1, p4lb, mlb)

    for pol_lb in [-1, 1] : 
      for pol_p in [-1, 1] : 
        ampl = Complex(Const(0.), Const(0.))
        for r,s in zip(resonances, switches) : 
          lineshape = r[0]
          spin = r[1]
          parity = r[2]
          if pol_p == -1 : 
            sign = CouplingSign(spin, parity)
            coupling1 = r[3]*sign
            coupling2 = r[4]*sign
          else : 
            coupling1 = r[3]
            coupling2 = r[4]
          ampl += Complex(s, Const(0.))*coupling1*lineshape*\
                  CovariantBaryonDecayAmplitude(p4p, p4d, p4pi, p4lb, spinor_p[pol_p], spinor_lb[pol_lb], spin, parity,  1, cache = cache)
          ampl += Complex(s, Const(0.))*coupling2*lineshape*\
                  CovariantBaryonDecayAmplitude(p4p, p4d, p4pi, p4lb, spinor_p[pol_p], spinor_lb[pol_lb], spin, parity, -1, cache = cache)
        density += Density(ampl)

    return density

  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  data_model = pdf(data_ph)
  norm_model = pdf(norm_ph)

  tf.set_random_seed(1)
  sess = tf.Session()

  init = tf.global_variables_initializer()
  sess.run(init)

  norm_sample = sess.run( phsp.RectangularGridSample(norm_grid, norm_grid) )
  print "Normalisation sample size = ", len(norm_sample)

  majorant = EstimateMaximum(sess, data_model, data_ph, norm_sample )*1.5
  print "Maximum = ", majorant
  data_sample = RunToyMC( sess, data_model, data_ph, phsp, toy_sample, majorant, chunk = 50000, switches = switches)

  f = TFile.Open("toy.root", "RECREATE")
  FillNTuple("toy", data_sample, ["m2dp", "m2ppi" ] + [ "w%d" % (n+1) for n in range(len(switches)) ])
  f.Close()

  ff = CalculateFitFractions(sess, data_model, data_ph, switches, norm_sample = norm_sample)
  WriteFitFractions(ff, ["Lc2880", "LcX", "Lc2940", "NR12p", "NR12m", "NR32p", "NR32m"], "fitfractions_init.txt")

  nll = UnbinnedNLL( data_model, Integral( norm_model ) )
  
  start = timer()
  result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = gradient )
  end = timer()
  print(end - start) 
  print result
  WriteFitResults(result, "result.txt")

  ff = CalculateFitFractions(sess, data_model, data_ph, switches, norm_sample = norm_sample)
  WriteFitFractions(ff, ["Lc2880", "LcX", "Lc2940", "NR12p", "NR12m", "NR32p", "NR32m"], "fitfractions.txt")

  fit_sample = RunToyMC(sess, data_model, data_ph, phsp, 100000, majorant, chunk = 50000, switches = switches)
  f = TFile.Open("toyresult.root", "RECREATE")
  FillNTuple("toy", fit_sample, ["m2dp", "m2ppi" ] + [ "w%d" % (n+1) for n in range(len(switches)) ])
  f.Close()

  h1 = TH2F("h1", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  h2 = TH2F("h2", "", 100, phsp.minab-0.2, phsp.maxab+0.2, 100, phsp.minbc-0.2 , phsp.maxbc+0.2 ) 
  h3 = TH1F("h3", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  h4 = TH1F("h4", "", 100, phsp.minab-0.2, phsp.maxab+0.2 ) 
  h5 = TH1F("h5", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 
  h6 = TH1F("h6", "", 100, phsp.minbc-0.2, phsp.maxbc+0.2 ) 

  for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

  for f in fit_sample : 
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])

  c = TCanvas("c","", 600, 600)
  c.Divide(2, 2)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  c.cd(1); h1.Draw("zcol")
  c.cd(2); h2.Draw("zcol")
  c.cd(3); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
  c.cd(4); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
  c.Update()

  print(end - start) 

  f = open("timing.txt","a")
  f.write(str(sys.argv) + " : %f sec, %d iterations\n" % (end-start, result["iterations"]))
  f.close()


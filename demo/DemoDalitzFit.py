# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf
import array

from ROOT import TH1F, TH2F, TVirtualFitter, TCanvas

fptype = tf.float64

def HelicityAngle(m2ab, m2bc, md, ma, mb, mc) :
  md2 = md**2
  ma2 = ma**2
  mb2 = mb**2
  mc2 = mc**2
  m2ac = md2 + ma2 + mb2 + mc2 - m2ab - m2bc
  mab = tf.sqrt(m2ab)
  mac = tf.sqrt(m2ac)
  mbc = tf.sqrt(m2bc)
  p2a = 0.25/md2*(md2-(mbc+ma)**2)*(md2-(mbc-ma)**2)
  p2b = 0.25/md2*(md2-(mac+mb)**2)*(md2-(mac-mb)**2)
  p2c = 0.25/md2*(md2-(mab+mc)**2)*(md2-(mab-mc)**2)
  eb = (m2ab-ma2+mb2)/2./mab
  ec = (md2-m2ab-mc2)/2./mab
  pb = tf.sqrt(eb**2-mb2)
  pc = tf.sqrt(ec**2-mc2)
  e2sum = (eb+ec)**2
  m2bc_max = e2sum-(pb-pc)**2
  m2bc_min = e2sum-(pb+pc)**2
  return (m2bc_max + m2bc_min - 2.*m2bc)/(m2bc_max-m2bc_min)

class DalitzPhaseSpace : 

  def __init__(self, ma, mb, mc, md) : 
    self.ma = ma
    self.mb = mb
    self.mc = mc
    self.md = md
    self.ma2 = ma**2
    self.mb2 = mb**2
    self.mc2 = mc**2
    self.md2 = md**2
    self.msqsum = self.md2 + self.ma2 + self.mb2 + self.mc2
    self.minab = (ma + mb)**2
    self.maxab = (md - mc)**2
    self.minbc = (mb + mc)**2
    self.maxbc = (md - ma)**2

  def Inside(self, m2ab, m2bc) : 
    inside = tf.logical_and(tf.logical_and(tf.greater(m2ab, self.minab), tf.less(m2ab, self.maxab)), \
                            tf.logical_and(tf.greater(m2bc, self.minbc), tf.less(m2bc, self.maxbc)))
    eb = (m2ab - self.ma2 + self.mb2)/2./tf.sqrt(m2ab)
    ec = (self.md2 - m2ab - self.mc2)/2./tf.sqrt(m2ab)
    p2b = eb**2 - self.mb2
    p2c = ec**2 - self.mc2
    inside = tf.logical_and(inside, tf.logical_and(tf.greater(p2c, 0), tf.greater(p2b, 0)))
    m2bc_max = (eb+ec)**2 - (tf.sqrt(p2b) - tf.sqrt(p2c))**2
    m2bc_min = (eb+ec)**2 - (tf.sqrt(p2b) + tf.sqrt(p2c))**2
    return tf.logical_and(inside, tf.logical_and(tf.greater(m2bc, m2bc_min), tf.less(m2bc, m2bc_max) ) )

  def UniformSample(self, size, majorant = -1) : 
    v = [ tf.random_uniform([size], self.minab, self.maxab, dtype=fptype), 
          tf.random_uniform([size], self.minbc, self.maxbc, dtype=fptype) ] 
    if majorant>0 : v += [ tf.random_uniform([size], 0., majorant, dtype=fptype) ]
    dlz = tf.stack(v, axis=1)
    return tf.boolean_mask(dlz, self.Inside(v[0], v[1]) )

  def M2ab(self, sample) : return tf.transpose(sample)[0]

  def M2bc(self, sample) : return tf.transpose(sample)[1]

  def M2ac(self, sample) : return self.msqsum - self.M2ab(sample) - self.M2bc(sample)

  def HelicityAB(self, sample) : 
    return HelicityAngle(self.M2ab(sample), self.M2bc(sample), self.md, self.ma, self.mb, self.mc)

  def HelicityBC(self, sample) : 
    return HelicityAngle(self.M2bc(sample), self.M2ac(sample), self.md, self.mb, self.mc, self.ma)

  def HelicityAC(self, sample) : 
    return HelicityAngle(self.M2ac(sample), self.M2ab(sample), self.md, self.mc, self.ma, self.mb)

def Density(ampl) : return tf.abs(ampl)**2

def Complex(re, im) : return tf.complex(re, im)

def Const(c) : return tf.constant(c, dtype=fptype)

def RelativisticBreitWigner(m2ab, mres, wres) : 
  return 1./Complex(mres**2-m2ab, -mres*wres)

def HelicityAmplitude(x, spin) : 
  if spin == 0 : return Complex(Const(1.), Const(0.))
  if spin == 1 : return Complex(x, Const(0.))
  if spin == 2 : return Complex(x**2-1./3., Const(0.))
  if spin == 3 : return Complex(x**3-3./5.*x, Const(0.))
  return None

def AcceptRejectSample(density, phsp, size, majorant) : 
  sample = phsp.UniformSample(size, majorant)
  x = tf.transpose(tf.transpose(sample)[:-1])
  r = tf.transpose(sample)[-1]
  return tf.boolean_mask(x, density(x)>r)

def EstimateMaximum(density, phsp, size) : 
  return tf.reduce_max( density(phsp.UniformSample(size)) )

class FitParameter(tf.Variable) : 
  def __init__(self, name, init_value, step_size, lower_limit, upper_limit) : 
    tf.Variable.__init__(self, init_value, dtype = fptype)
    self.init_value = init_value
    self.par_name = name
    self.step_size = step_size
    self.lower_limit = lower_limit
    self.upper_limit = upper_limit
    self.placeholder = tf.placeholder(self.dtype, shape=self.get_shape())
    self.update_op = self.assign(self.placeholder)
    self.prev_value = None

  def update(self, session, value) : 
    if value != self.prev_value : 
      session.run( self.update_op, { self.placeholder : value } )
      self.prev_value = value

  def floating(self) : return self.step_size > 0

def UnbinnedLogLikelihood(pdf, data_sample, integ_sample) :
  norm = tf.reduce_sum(pdf(integ_sample))
  return -tf.reduce_sum(tf.log(pdf(data_sample)/norm ))

def MinuitFit(sess, model, data_sample, integ_sample, call_limit = 50000) :

  tfpars = tf.trainable_variables()  # Create TF variables
  float_tfpars = [ p for p in tfpars if p.floating() ]

  nll = UnbinnedLogLikelihood(model, data_sample, integ_sample)
  gradient = tf.gradients(nll, float_tfpars)

  init = tf.initialize_all_variables()
  sess.run(init)

  def fcn(npar, gin, f, par, istatus) :
    for i,p in enumerate(float_tfpars) : p.update(sess, par[i])
    f[0] = sess.run(nll)         # Calculate log likelihood
    if istatus == 2 :            # If gradient calculation is needed
      dnll = sess.run(gradient)  # Calculate analytic gradient
      for i in range(len(float_tfpars)) : gin[i] = dnll[i] # Pass gradient to MINUIT
    fcn.n += 1
    if fcn.n % 10 == 0 : print fcn.n, istatus, f[0], sess.run(tfpars)

  fcn.n = 0
  minuit = TVirtualFitter.Fitter(0, len(float_tfpars))
  minuit.Clear()
  minuit.SetFCN(fcn)
  arglist = array.array('d', 10*[0])

  for n,p in enumerate(float_tfpars) : 
    minuit.SetParameter(n, p.par_name, p.init_value, p.step_size, p.lower_limit, p.upper_limit)

  minuit.ExecuteCommand("SET GRA", arglist, 0)
  arglist[0] = call_limit
  minuit.ExecuteCommand("MIGRAD", arglist, 1)

if __name__ == "__main__" : 

  dlz = DalitzPhaseSpace(0.139, 0.497, 0.139, 1.869)

  uniform_sample = dlz.UniformSample(1000000)

  mass   = Const(0.892)
  width  = Const(0.050)
  mass3  = FitParameter("mass3",  0.770, 0.010, 0.6, 0.9)
  width3 = FitParameter("width3", 0.150, 0.010, 0.0, 0.2)

  a1 = Complex(Const(1.0), Const(0.0))
  a2 = Complex(FitParameter("a2r", -0.1, 0.01, -10., 10.), FitParameter("a2i", 0., 0.01, -10., 10.) )
  a3 = Complex(FitParameter("a3r", 0., 0.01, -10., 10.), FitParameter("a3i", 2., 0.01, -10., 10.) )

  def model(x) : 
    ampl1 = a1*RelativisticBreitWigner(dlz.M2ab(x), mass, width)*HelicityAmplitude(dlz.HelicityAB(x), 1)
    ampl2 = a2*RelativisticBreitWigner(dlz.M2bc(x), mass, width)*HelicityAmplitude(dlz.HelicityBC(x), 1)
    ampl3 = a3*RelativisticBreitWigner(dlz.M2ac(x), mass3, width3)*HelicityAmplitude(dlz.HelicityAC(x), 1)
    return Density( ampl1 + ampl2 + ampl3 )

  tf.set_random_seed(1)
  sess = tf.Session()

  init = tf.initialize_all_variables()
  sess.run(init)

  integ_data = sess.run(uniform_sample)
  majorant = sess.run( EstimateMaximum(model, dlz, 100000) )*1.1
  print "Maximum = ", majorant

  gen_sample = AcceptRejectSample(model, dlz, 100000, majorant)
  data = sess.run(gen_sample)

  MinuitFit(sess, model, data, integ_data)

  fit_data = sess.run( AcceptRejectSample(model, dlz, 10000000, majorant) )

  print len(data), len(integ_data), len(fit_data)

  h1 = TH2F("h1", "", 100, dlz.minab-0.2, dlz.maxab+0.2, 100, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h2 = TH2F("h2", "", 100, dlz.minab-0.2, dlz.maxab+0.2, 100, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h3 = TH1F("h3", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h4 = TH1F("h4", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h5 = TH1F("h5", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 
  h6 = TH1F("h6", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 

  for d in data : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

  for f in fit_data : 
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])

  c = TCanvas("c","", 600, 600)
  c.Divide(2, 2)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  c.cd(1); h1.Draw("zcol")
  c.cd(2); h2.Draw("zcol")
  c.cd(3); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
  c.cd(4); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
  c.Update()
